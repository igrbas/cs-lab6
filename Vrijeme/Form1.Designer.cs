﻿
namespace Vrijeme {
    partial class Form {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.cobCity = new System.Windows.Forms.ComboBox();
            this.lbCity = new System.Windows.Forms.Label();
            this.lbTemperature = new System.Windows.Forms.Label();
            this.lbVlaga = new System.Windows.Forms.Label();
            this.lbTlak = new System.Windows.Forms.Label();
            this.lbWarm = new System.Windows.Forms.Label();
            this.lbCold = new System.Windows.Forms.Label();
            this.libWarmCity = new System.Windows.Forms.ListBox();
            this.libColdCity = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // cobCity
            // 
            this.cobCity.FormattingEnabled = true;
            this.cobCity.Location = new System.Drawing.Point(318, 32);
            this.cobCity.Name = "cobCity";
            this.cobCity.Size = new System.Drawing.Size(121, 23);
            this.cobCity.TabIndex = 0;
            this.cobCity.TextChanged += new System.EventHandler(this.cobCity_TextChanged);
            // 
            // lbCity
            // 
            this.lbCity.AutoSize = true;
            this.lbCity.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbCity.Location = new System.Drawing.Point(330, 79);
            this.lbCity.Name = "lbCity";
            this.lbCity.Size = new System.Drawing.Size(0, 37);
            this.lbCity.TabIndex = 1;
            // 
            // lbTemperature
            // 
            this.lbTemperature.AutoSize = true;
            this.lbTemperature.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbTemperature.Location = new System.Drawing.Point(347, 116);
            this.lbTemperature.Name = "lbTemperature";
            this.lbTemperature.Size = new System.Drawing.Size(44, 37);
            this.lbTemperature.TabIndex = 2;
            this.lbTemperature.Text = "°C";
            // 
            // lbVlaga
            // 
            this.lbVlaga.AutoSize = true;
            this.lbVlaga.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbVlaga.Location = new System.Drawing.Point(352, 170);
            this.lbVlaga.Name = "lbVlaga";
            this.lbVlaga.Size = new System.Drawing.Size(39, 37);
            this.lbVlaga.TabIndex = 3;
            this.lbVlaga.Text = "%";
            // 
            // lbTlak
            // 
            this.lbTlak.AutoSize = true;
            this.lbTlak.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbTlak.Location = new System.Drawing.Point(340, 207);
            this.lbTlak.Name = "lbTlak";
            this.lbTlak.Size = new System.Drawing.Size(60, 37);
            this.lbTlak.TabIndex = 4;
            this.lbTlak.Text = "hPa";
            // 
            // lbWarm
            // 
            this.lbWarm.AutoSize = true;
            this.lbWarm.Location = new System.Drawing.Point(116, 275);
            this.lbWarm.Name = "lbWarm";
            this.lbWarm.Size = new System.Drawing.Size(98, 15);
            this.lbWarm.TabIndex = 5;
            this.lbWarm.Text = "Najtopliji gradovi";
            // 
            // lbCold
            // 
            this.lbCold.AutoSize = true;
            this.lbCold.Location = new System.Drawing.Point(589, 275);
            this.lbCold.Name = "lbCold";
            this.lbCold.Size = new System.Drawing.Size(107, 15);
            this.lbCold.TabIndex = 6;
            this.lbCold.Text = "Najhladniji gradovi";
            // 
            // libWarmCity
            // 
            this.libWarmCity.FormattingEnabled = true;
            this.libWarmCity.ItemHeight = 15;
            this.libWarmCity.Location = new System.Drawing.Point(64, 314);
            this.libWarmCity.Name = "libWarmCity";
            this.libWarmCity.Size = new System.Drawing.Size(196, 94);
            this.libWarmCity.TabIndex = 7;
            // 
            // libColdCity
            // 
            this.libColdCity.FormattingEnabled = true;
            this.libColdCity.ItemHeight = 15;
            this.libColdCity.Location = new System.Drawing.Point(516, 314);
            this.libColdCity.Name = "libColdCity";
            this.libColdCity.Size = new System.Drawing.Size(231, 94);
            this.libColdCity.TabIndex = 8;
            // 
            // timer1
            // 
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.libColdCity);
            this.Controls.Add(this.libWarmCity);
            this.Controls.Add(this.lbCold);
            this.Controls.Add(this.lbWarm);
            this.Controls.Add(this.lbTlak);
            this.Controls.Add(this.lbVlaga);
            this.Controls.Add(this.lbTemperature);
            this.Controls.Add(this.lbCity);
            this.Controls.Add(this.cobCity);
            this.Name = "Form";
            this.Text = "Vrijeme";
            this.Load += new System.EventHandler(this.Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cobCity;
        private System.Windows.Forms.Label lbCity;
        private System.Windows.Forms.Label lbTemperature;
        private System.Windows.Forms.Label lbVlaga;
        private System.Windows.Forms.Label lbTlak;
        private System.Windows.Forms.Label lbWarm;
        private System.Windows.Forms.Label lbCold;
        private System.Windows.Forms.ListBox libWarmCity;
        private System.Windows.Forms.ListBox libColdCity;
        private System.Windows.Forms.Timer timer1;
    }
}

