﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Vrijeme {
    class Vrijeme {
        public Vrijeme() {
            vrijeme = new XmlDocument();
            vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");
            DohvatiPodatke();
        }

        public List<VrijemePodatak> DohvatiPodatke() {
            podaci = new List<VrijemePodatak>();
            XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
            foreach (XmlNode grad in gradovi) {
                podaci.Add(new VrijemePodatak(
                    grad["GradIme"].InnerText,
                    grad["Podatci"]["Temp"].InnerText,
                    grad["Podatci"]["Vlaga"].InnerText,
                    grad["Podatci"]["Tlak"].InnerText
                ));
            }
            return podaci;
        }

        public IEnumerable<string> DohvatiGradove() {
            return
                from podatak in podaci
                orderby podatak.Grad
                select podatak.Grad;
        }

        public IEnumerable<string> DohvatiNajtoplije() {
            return
                (from podatak in podaci
                 orderby podatak.Temperatura descending
                 select podatak.Grad).Take(5);
        }

        public IEnumerable<string> DohvatiNajhladnije() {
            return
                (from podatak in podaci
                orderby podatak.Temperatura ascending
                select podatak.Grad).Take(5);
        }

        public VrijemePodatak DohvatiPodatkeZaGrad(string grad) {
            return
                (from podatak in podaci
                where podatak.Grad == grad
                select podatak).First();
        }

        private List<VrijemePodatak> podaci;
        private XmlDocument vrijeme;
    }
}
