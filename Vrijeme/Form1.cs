﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vrijeme {
    public partial class Form : System.Windows.Forms.Form {
        Vrijeme vrijeme;
        public Form() {
            InitializeComponent();
            vrijeme = new Vrijeme();
        }

        private void Form_Load(object sender, EventArgs e) {
            cobCity.Items.AddRange(vrijeme.DohvatiGradove().ToArray());
            libWarmCity.Items.AddRange(vrijeme.DohvatiNajtoplije().ToArray());
            libColdCity.Items.AddRange(vrijeme.DohvatiNajhladnije().ToArray());
        }

        private void cobCity_TextChanged(object sender, EventArgs e) {
            var podaci = vrijeme.DohvatiPodatkeZaGrad(cobCity.Text);
            OsvjeziPrikaz();
            lbCity.Text = podaci.Grad;
            lbTemperature.Text = podaci.Temperatura + " °C";
            lbVlaga.Text = podaci.Vlaga + " %";
            lbTlak.Text = podaci.Tlak + " hPa";
        }

        private void OsvjeziPrikaz() {
            libWarmCity.Items.Clear();
            libColdCity.Items.Clear();
            libWarmCity.Items.AddRange(vrijeme.DohvatiNajtoplije().ToArray());
            libColdCity.Items.AddRange(vrijeme.DohvatiNajhladnije().ToArray());
        }

        private void timer1_Tick(object sender, EventArgs e) {
            OsvjeziPrikaz();
        }
    }
}
